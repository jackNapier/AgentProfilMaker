#!/usr/bin/python
# coding=UTF-8

from PIL import Image
from datetime import datetime
import glob, os
import time

#Name des Agent
user = "r3f1zul"

#false wenn eine stats.png vom Agent in profil/<user> hinterlegt ist
#ansonsten true
privat = "false" 


## !!! Ab hier keine Anpassungen machen !!!
t0 = time.clock()
width = 0
height = 0
lines = 0

img=Image.open('profil/' + user + '/profil.png').convert('RGB')
print (img.mode)
width,height = img.size

width = width - 20

for infile in glob.glob("Missions/"+ user + "/*/**/*.png", recursive=True):
	im = Image.open(infile)
	height = height + im.size[1]
	lines = lines + 1

if privat != "true":
    img3=Image.open('profil/' + user + '/stats.png')
    height = height + img3.size[1]
else:
    height

new_image = Image.open("profil/" + user + "/profil.png")
new_image = Image.new("RGB", (width, height))
out_image = user + " AgentProfil "

new_image.paste(img,(0,0))
nh = img.size[1]

dirList = glob.glob("Missions/"+ user + "/*/**/*.png", recursive=True)
#dirList.sort()
dirList.sort(reverse=True)

for number, infile in enumerate(dirList):
    #file, ext = os.path.splitext(infile)
    print (infile)
    im = Image.open(infile)
    new_image.paste(im,(0,nh))
    nh = nh + im.size[1]
    #new_image.show()


if privat != "true":
    new_image.paste(img3,(0,nh))

if not os.path.exists("output/" + user):
    				os.makedirs("output/" + user)

out = "output/" + user + "/" + out_image + str(datetime.now()) +".png"

new_image.save(out)

print ("Es wurden " + str(lines) + " Missionsreihen gezählt")
print (time.clock() - t0)
