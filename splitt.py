from PIL import Image
from datetime import datetime
import glob, os
import time
import math

#dirNumber beim erstenmal mit 0 durchlaufen lassen
# bei späteren Aktualisierung des Profiles 0 durch die Gesamtzahl der Ordner im Mission/<user> + 1 ersetzen
dirNumber = 0

#dient zum ablegen der Missionen für mehrere Agentprofilen
user = "r3f1zul"

#cropsize: Aufgrund unterschiedlicher Auflösungen müsst ihr mal bei einem Screenshot testen bei welchen Wert die erstellten Bildausschnitte genau vor der nächsten Missionsreihe enden.
cropsize = 194


## !!!! Ab hier keine Anpassungen machen !!!
t0 = time.clock()
width = 0
height = 0
count = 0

tmp = 0
x = 0
privat = 0

dirList1 = glob.glob("tmp/*.png")
dirList1.sort(reverse=True)

pngCounter = len(glob.glob("tmp/*.png"))


for number, infile in enumerate(dirList1):
	file, ext = os.path.splitext(infile)
	im = Image.open(infile).convert('RGB')
	width, height = im.size
	h = height
	test = im

	r, g, b = im.split()

	#r.save("tt_r.png")
	#g.save("tt_g.png")
	
	count = 99
	print ("Bild " + str(number + 1) + " von " + str(pngCounter))
	for x in range(height):
		if r.getpixel((115,x)) > 150 and r.getpixel((115,x))<250 and g.getpixel((115,x)) > 150 and g.getpixel((115,x)) < 175:
			if r.getpixel((115,x-1)) < 150:
				if abs(x-tmp) != 1 and abs(x-tmp) != 2 and height-x > 160:
					p = x
					if dirNumber < 10:
						path = "Missions/" + user + "/00000" +str(dirNumber)
					elif dirNumber <100:
						path = "Missions/" + user + "/0000" +str(dirNumber)
					elif dirNumber <1000:
						path = "Missions/" + user + "/000" +str(dirNumber)
					elif dirNumber <10000:
						path = "Missions/" + user + "/00" +str(dirNumber)
					elif dirNumber <100000:
						path = "Missions/" + user + "/0" +str(dirNumber)
					if not os.path.exists(path):
    						os.makedirs(path)
					if count < 10:
						strCounter = "000" + str(count)
					elif count <100:
						strCounter = "00" + str(count)
					elif count <1000:
						strCounter = "0" + str(count)
					test.crop((0,p-1,width,p+cropsize)).save( path +"/ausschnitt"+strCounter+".png")
					tmp = x
					count = count - 1
	dirNumber = dirNumber + 1
print (str(time.clock() - t0) + "s")

